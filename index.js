let http = require('http');
let port = 3000; 
let client = "Miggy"

http.createServer( function(request , response) {
	if (request.url === '/') {
		response.write('Welcome!');
		response.end();
	} else if (request.url === '/profile') {
		response.write(`${client}'s profile.`);
		response.end();
	} else if (request.url === '/register') {
		response.write(`Register your account here!`);
		response.end();
	} else if (request.url === '/login') {
		response.write(`Login or Sign Up Here!`);
		response.end();
	} else {
		response.write('Error: Page was not found.');
		response.end();
	}
}).listen(port);

console.log(`Port Initialized: ${port}`);